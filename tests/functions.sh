#!/bin/bash
set -euo pipefail

source scripts/functions.sh
source tests/helpers.sh

say "functions"

_failed_init

function check_owner_repo {
    declare -A OWNER_REPOS

    OWNER_REPOS[http://git-extensions-are-stripped/a/b/c.git]=b.c
    OWNER_REPOS[http://extension-and-slashes-are-stripped/a/b/c.git/]=b.c
    OWNER_REPOS[http://slashes-are-stripped/a/b/c/]=b.c
    OWNER_REPOS[http://two-directories/a/b/c]=b.c
    OWNER_REPOS[http://one-directory/a/b]=a.b
    OWNER_REPOS[http://no-directory/a]=git.a
    OWNER_REPOS[http://UPPER-CASE/A/B/C]=b.c

    OWNER_REPOS[git://host.com/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[http://host.com/git/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[git://host.com/name2.git]=git.name2
    OWNER_REPOS[http://host.com/git/name2.git]=git.name2
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git]=arm64.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git]=davem.net-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/jejb/scsi.git]=jejb.scsi
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git]=next.linux-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux.git]=powerpc.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git]=rdma.rdma
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git]=rt.linux-rt-devel
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/sashal/linux-stable.git]=sashal.linux-stable
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git]=stable.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git]=stable.linux-stable-rc
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/stable-queue.git]=stable.stable-queue
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git]=torvalds.linux
    OWNER_REPOS[https://gitlab.com/CKI-project/kernel-ark.git]=cki-project.kernel-ark
    OWNER_REPOS[https://host.com/gerrit/kernel-name3]=gerrit.kernel-name3
    OWNER_REPOS[https://host.com/gerrit/kernel-name4.git]=gerrit.kernel-name4

    for URL in "${!OWNER_REPOS[@]}"; do
        _check_equal "$(get_owner_repo "${URL}")" "${OWNER_REPOS[${URL}]}" "owner.repo" "Is the owner.repo correct for $URL"
    done
}
check_owner_repo

function check_get_auth_git_url() {
    export GITLAB_READ_REPO_TOKENS="$3"
    export READ_TOKEN=$4
    _check_equal "$(get_auth_git_url "$1")" "$2" "get_auth_git_url" "Is the auth URL correct for $1"
}
# Test no auth
check_get_auth_git_url "https://gitlab.example/example/repo" "https://gitlab.example/example/repo" "" "-"
# Test correct auth URL
check_get_auth_git_url "https://gitlab.example/example/repo" "https://oauth2:token@gitlab.example/example/repo" '{"gitlab.example": "READ_TOKEN"}' "token"
# Test auth for a different GitLab instance - thus no auth needed for our URL
check_get_auth_git_url "https://gitlab.different/example/repo" "https://gitlab.different/example/repo" '{"gitlab.example": "READ_TOKEN"}' "token"

# function() instead of function{} so that the stub functions are scoped
function check_git_cache_clone()
(
    declare aws_s3_download_params
    function git {
        /usr/bin/git "$@" > /dev/null 2>&1
        echo "git_params='$*'" >&2
    }
    function aws_s3_download {
        mkdir -p test-output/git-repo
        /usr/bin/git init test-output/git-repo > /dev/null 2>&1
        tar -C test-output/git-repo -cf - .
        echo "aws_s3_download_params='$*'" >&2
    }
    trap 'rm -rf test-output/' EXIT
    local GIT_CACHE_DIR="test-output/git-cache"
    eval "$(git_cache_clone https://host.com/a/b/repo.git tests/workdir --quiet 2>&1 >/dev/null)"
    git_params_array=($git_params)
    aws_s3_download_params_array=($aws_s3_download_params)

    _check_equal "${aws_s3_download_params_array[0]}" "BUCKET_GIT_CACHE" "bucket" "Does git_cache_clone pass the bucket correctly into aws_s3_download"
    _check_equal "${aws_s3_download_params_array[1]}" "b.repo.tar" "tarfile" "Does git_cache_clone pass the tar file name correctly into aws_s3_download"
    _check_equal "${git_params_array[-1]}" "--quiet" "param" "Does git_cache_clone pass the additional parameters to git clone"
    _check_equal "$(stat test-output/git-repo/.git/config > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"
)
check_git_cache_clone

# function() instead of function{} so that the stub functions are scoped
function check_cpu_count()
(
    local NPROC_RESULT="$1"
    local CFS_QUOTA_RESULT="$2"
    local EXPECTED_CPUS_AVAILABLE="$3"
    local EXPECTED_MAKE_JOBS="$4"
    local EXPECTED_RPM_BUILD_NCPUS="$5"
    local MESSAGE="$6"
    function nproc {
        echo "${NPROC_RESULT}"
    }
    function get_cfs_quota {
        echo "${CFS_QUOTA_RESULT}"
    }
    eval "$(get_cpu_count)"

    _check_equal "${CPUS_AVAILABLE}" "${EXPECTED_CPUS_AVAILABLE}" cpu_count "Is the CPU count correct with ${MESSAGE}"
    _check_equal "${MAKE_JOBS}" "${EXPECTED_MAKE_JOBS}" make_jobs "Is the make job count correct with ${MESSAGE}"
    _check_equal "${RPM_BUILD_NCPUS}" "${EXPECTED_RPM_BUILD_NCPUS}" rpm_build_ncpus "Is the RPM build CPU count correct with ${MESSAGE}"
)
check_cpu_count 1000 800000 8 12 12 "a cgroup limit"
check_cpu_count 1000 50000 1 1 1 "a cgroup limit for less than 1 cpu"
check_cpu_count 1000 "-1" 1000 1500 1500 "an unbounded cgroup limit"
check_cpu_count 1000 "" 1000 1500 1500 "no cgroup limit file"

function check_kpet_variable_arguments() {
    local MESSAGE="$1"
    local EXPECTED_COUNT="$2"
    shift 2
    local KPET_VARIABLES=''
    local KPET_ADDITIONAL_VARIABLES=''
    for variable_item in "$@"; do
        variable_key="${variable_item%%=*}"
        variable_value="${variable_item#*=}"
        KPET_VARIABLES="${KPET_VARIABLES} ${variable_key}"
        if [ -n "${variable_value}" ]; then
            local "${variable_item}"
        fi
    done
    kpet_generate_variable_arguments
    _check_equal "${#kpet_variable_arguments[@]}" "$((2 * EXPECTED_COUNT))" "argument count" "Is the number of arguments correct with ${MESSAGE}"
    local -i index=0
    while [ "${index}" -lt "${#kpet_variable_arguments[@]}" ]; do
        _check_equal "${kpet_variable_arguments[${index}]}" "-v" "argument" "Is argument $((index + 1)) as expected with ${MESSAGE}"
        index+=1
        _check_equal "${kpet_variable_arguments[${index}]}" "variable$((index / 2 + 1))_key=value $((index / 2 + 1))" "argument" "Is argument $((index + 1)) as expected with ${MESSAGE}"
        index+=1
    done
}
check_kpet_variable_arguments "no variables" 0
check_kpet_variable_arguments "a non-existent variable" 0 "variable1_key="
check_kpet_variable_arguments "a variable with spaces" 1 "variable1_key=value 1"
check_kpet_variable_arguments "multiple variables" 2 "variable1_key=value 1" "variable2_key=value 2"

function check_is_true() {
    declare -A IS_TRUE_MAPPING
    IS_TRUE_MAPPING["true"]=0
    IS_TRUE_MAPPING["True"]=0
    IS_TRUE_MAPPING["false"]=1
    IS_TRUE_MAPPING["False"]=1
    IS_TRUE_MAPPING["randomvalue"]=1

    for VALUE in "${!IS_TRUE_MAPPING[@]}"; do
        is_true ${VALUE} && RETURNED=0 || RETURNED=$?
        _check_equal "${RETURNED}" "${IS_TRUE_MAPPING[${VALUE}]}" "is_true returned" "Is the is_true value correct for $VALUE"
    done
}
check_is_true

function check_git_url() {
    _check_equal "$(git_clean_url "$1")" "$2" "git_clean_url" "Is the git_clean_url $3"
}
check_git_url "https://foo.bar/test.git/" "https://foo.bar/test.git/" "passing through correct URLs"
check_git_url "https://foo.bar/test.git"  "https://foo.bar/test.git/" "appending a slash"
check_git_url "https://foo.bar/test"      "https://foo.bar/test.git/" "appending .git/"
check_git_url "https://foo.bar/test/"     "https://foo.bar/test.git/" "appending .git with an existing slash"
check_git_url "foo.bar/test.git/"         "https://foo.bar/test.git/" "prepending the protocol"

function loop_helper {
    STATE="$(cat /tmp/loop_state)"
    echo "$(("${STATE}" + 1))" > /tmp/loop_state
    echo loop$STATE-start >> /tmp/loop_log
    if [ $STATE -lt $1 ]; then
        false
    fi
    echo loop$STATE-end >> /tmp/loop_log
}

function check_loop_successful {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 3 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "3" "loops" "Is the command retried the correct number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "1" "completions" "Is the command completing exactly one     time"
    _check_equal "$s" "0" "exit code" "Does the loop command exit correctly"
}
check_loop_successful

function check_loop_failed {
    rm -f /tmp/loop_log /tmp/loop_state
    echo 1 > /tmp/loop_state
    loop_custom 5 0 loop_helper 10 &
    wait $! && s=0 || s=$?
    _check_equal "$(grep -c start /tmp/loop_log)" "5" "loops" "Is the command retried the maximum number     of times"
    _check_equal "$(grep -c end /tmp/loop_log)" "0" "completions" "Is the command never completing"
    _check_equal "$s" "1" "exit code" "Does the loop command fail correctly"
}
check_loop_failed

function check_dryrun_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/check_dryrun_by_upt.testfile
    local ARG_INPUT=/tmp/input.xml
    local ARG_OUTPUT=/tmp/output.yaml

    dryrun_by_upt "echo" "${ARG_INPUT}" "${ARG_OUTPUT}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m upt legacy convert \-i ${ARG_INPUT} \-r ${ARG_OUTPUT}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to UPT ok"

    CAPTURE=0
    grep "\-m restraint_wrap --help" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to restraint runner ok"
}
check_dryrun_by_upt

function check_dryrun_by_skt() {
    local ARG_INPUT=/tmp/input.xml
    local TEST_OUTPUT_FILE=/tmp/check_dryrun_by_skt.testfile

    dryrun_by_skt "echo" "echo" "${ARG_INPUT}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m skt.executable --help" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to SKT ok"

    CAPTURE=0
    grep "job-submit \-\-dry-run ${ARG_INPUT}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to bkr ok"
}
check_dryrun_by_skt

function check_test_by_skt() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_TIMEOUT_BIN=echo
    local ARG_TIMEOUT_LENGTH=400
    local ARG_PY3=python3
    local ARG_RC=/tmp/rc

    test_by_skt "${ARG_TIMEOUT_BIN}" "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" "${ARG_RC}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-v \-k 300 ${ARG_TIMEOUT_LENGTH} ${ARG_PY3} \-m skt.executable \-vv \-\-state \-\-rc ${ARG_RC} run \-\-wait" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to SKT testing ok"
}
check_test_by_skt

function check_test_by_restraint_runner() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_TIMEOUT_BIN=echo
    local ARG_TIMEOUT_LENGTH=400
    local ARG_PY3=echo
    local ARG_RC=/tmp/rc
    local ARG_UPT_INPUT=/tmp/input.xml
    local ARG_UPT_OUTPUT_DIR=/tmp/output.yaml
    local ARG_EXIT_CMD=echo

    test_by_restraint_runner "${ARG_TIMEOUT_BIN}" "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" "${ARG_RC}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_DIR}" "${ARG_EXIT_CMD}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-v \-k 300 400 ${ARG_PY3} \-m restraint_wrap test \-\-rc ${ARG_RC} \-\-reruns 1 \-i ${ARG_UPT_INPUT} \-o ${ARG_UPT_OUTPUT_DIR}/run" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to restraint runner ok"

    test_by_restraint_runner "blah-this-must-fail" "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" "${ARG_RC}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_DIR}" "${ARG_EXIT_CMD}" > "${TEST_OUTPUT_FILE}" 2>/dev/null || CAPTURE=$?
    local CAPTURE=0
    grep "127" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Does invalid restraint invocation exit"
}
check_test_by_restraint_runner

function check_upt_release_testing_resources_on_fatal_err() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_RETCODE=1
    local ARG_PY3=echo
    local ARG_EXIT_CMD=echo
    local ARG_UPT_INPUT=/tmp/input.xml

    upt_release_testing_resources_on_fatal_err "${ARG_RETCODE}" "${ARG_PY3}" "${ARG_UPT_INPUT}" "${ARG_EXIT_CMD}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-m upt cancel \-r ${ARG_UPT_INPUT}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt cancel ok"

    local ARG_RETCODE=0
    local CAPTURE=0
    upt_release_testing_resources_on_fatal_err "${ARG_RETCODE}" "${ARG_PY3}" "${ARG_UPT_INPUT}" "${ARG_EXIT_CMD}" > "${TEST_OUTPUT_FILE}" || true

    test ! -s "${TEST_OUTPUT_FILE}" || CAPTURE=1
    _check_equal "${CAPTURE}" 0 "Retcode" "upt cancel doesn't execute on zero retcode"
}
check_upt_release_testing_resources_on_fatal_err

function check_provision_by_upt() {
    local TEST_OUTPUT_FILE=/tmp/testfile

    local ARG_TIMEOUT_BIN=echo
    local ARG_TIMEOUT_LENGTH=400
    local ARG_PY3=python3
    local ARG_RC=/tmp/rc
    local ARG_UPT_INPUT=/tmp/input.xml
    local ARG_UPT_OUTPUT_YAML=/tmp/output.yaml
    local ARG_EXIT_CMD=echo

    provision_by_upt "${ARG_TIMEOUT_BIN}" "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" "${ARG_RC}" "${ARG_UPT_INPUT}" "${ARG_UPT_OUTPUT_YAML}" "${ARG_EXIT_CMD}" > "${TEST_OUTPUT_FILE}" || true

    local CAPTURE=0
    grep "\-v \-k 300 ${ARG_TIMEOUT_LENGTH} ${ARG_PY3} \-m upt \-\-rc ${ARG_RC} legacy provision \-i ${ARG_UPT_INPUT} \-r ${ARG_UPT_OUTPUT_YAML}" "${TEST_OUTPUT_FILE}" 1>/dev/null || CAPTURE=$?
    _check_equal "${CAPTURE}" 0 "Retcode" "Are params passed to upt provision ok"
}
check_provision_by_upt

function check_get_successful_selftests() {
    _check_equal "$(get_successful_selftests "$1")" "$2" "get_successful_selftests" "Is the selftest list correct for $1"
}
# All successful
echo -e "net: 0\nbpf: 0\nlivepatch: 0\n" > /tmp/selftest-results1.yaml
check_get_successful_selftests "/tmp/selftest-results1.yaml" "TARGETS=net bpf livepatch"
# Some failed
echo -e "net: 0\nbpf: 2\nlivepatch: 0" > /tmp/selftest-results2.yaml
check_get_successful_selftests "/tmp/selftest-results2.yaml" "TARGETS=net livepatch"
# All failed
echo -e "net: 1\nbpf: 2\nlivepatch: 1" > /tmp/selftest-results3.yaml
check_get_successful_selftests "/tmp/selftest-results3.yaml" "TARGETS="

_failed_check
